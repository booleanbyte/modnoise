package com.booleanbyte.modnoise.util.math;

public class MathHelperScalar {
	
	public static double clamp(double a, double min, double max) {
		return Math.min(Math.max(a, min), max);
	}
	
	public static float clamp(float a, float min, float max) {
		return Math.min(Math.max(a, min), max);
	}
}
