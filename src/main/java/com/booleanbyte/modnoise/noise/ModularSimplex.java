package com.booleanbyte.modnoise.noise;

import com.booleanbyte.modnoise.AbstractModularnoise;
import com.booleanbyte.modnoise.util.math.MathHelperScalar;
import com.github.auburns.fastnoise.FastNoise;
import com.github.auburns.fastnoise.FastNoise.NoiseType;

public class ModularSimplex extends AbstractModularnoise {
	
	private final FastNoise simplex;
	
	public ModularSimplex(int seed, float scale) {
		simplex = new FastNoise(seed);
		simplex.SetNoiseType(NoiseType.Simplex);
		simplex.SetFrequency(1.0f / scale);
	}

	@Override
	public float getValueAt(float x, float y) {
		return (float) MathHelperScalar.clamp(simplex.GetNoise(x, y) * 0.5f + 0.5f, 0.0, 1.0);
	}
}
