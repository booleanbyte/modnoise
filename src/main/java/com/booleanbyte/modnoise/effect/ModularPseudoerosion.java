package com.booleanbyte.modnoise.effect;

import java.util.ArrayList;
import java.util.Random;

import com.booleanbyte.modnoise.AbstractModularnoise;
import com.booleanbyte.modnoise.util.math.MathHelperScalar;

public class ModularPseudoerosion extends AbstractModularnoise {
	private final AbstractModularnoise base;
	private float scale;
	private float strength;
	
	private int permutationSize = 256;
	private int repeat = permutationSize;
	private int[] dp = new int[permutationSize*2];
	
	public ModularPseudoerosion(int seed, float scale, float strength, AbstractModularnoise base) {
		this.base = base;
		this.scale = scale;
		this.strength = strength;
		
		int[] p = createPermutatationTable(permutationSize, seed);
		for(int pi = 0; pi < dp.length; pi++) {
			dp[pi] = p[pi%permutationSize];
		}
	}
	
	private int[] createPermutatationTable(int size, long seed) {
		//Create a random generator with supplied seed
		Random r = new Random(seed);
		
		//Generate a list containing every integer from 0 inclusive to size exlusive
		ArrayList<Integer> valueTabel = new ArrayList<Integer>();
		for(int i = 0; i < size; i++) {
			valueTabel.add(i);
		}
		
		//create the permutation table
		int[] permutationTable = new int[size];
		
		//Insert the values from the valueTable into the permutation table in a random order
		int pi = 0;
		while(valueTabel.size() > 0) {
			int index = r.nextInt(valueTabel.size());
			permutationTable[pi] = valueTabel.get(index);
			valueTabel.remove(index);
			pi++;
		}
		
		return permutationTable;
	}
	
	@Override
	public float getValueAt(float x, float y) {
		
		float b1 = base.getValueAt(x, y);
		float e1 = (float) macSearlasAt(x, y, base, scale, 1.0) * strength;
		return (float) MathHelperScalar.clamp(b1 + e1, 0.0, 1.0);
	}
	
	private double macSearlasAt(double globalx, double globaly, AbstractModularnoise baseNoise, double scale, double dispersion) {
		//Calculate the coordinates inside the repeating area
		double x = globalx / scale;
		double y = globaly / scale;
		if(repeat > 0) {
			if(x < 0) {
				x = repeat+(x%repeat);
			}
			else {
				x = x%repeat;
			}
			if(y < 0) {
				y = repeat+(y%repeat);
			}
			else {
				y = y%repeat;
			}
		}
		
		//Calculate the coordinates for the unit square that the coordinates is inside
		int xi = (int)x & 255;
		int yi = (int)y & 255;
		
		//Calculate the local coordinates inside the unit square
		double xf = x - (int)x;
		double yf = y - (int)y;
		
		//Generate the 25 closest points in global space
		double[] px = new double[25];
		double[] py = new double[25];
		float[] ph = new float[25];
		
		for(int ix = -2; ix <= 2; ix++) {
			for(int iy = -2; iy <= 2; iy++) {
				//The coordinates of the current unit square
				int cx = inc(xi, ix);
				int cy = inc(yi, iy);
				
				//The x and y hash for the unit square
				int xh = hashX(cx, cy);
				int yh = hashY(cx, cy);
				
				//The point offset in the unit square
				double xoffset = (double) xh/(double) repeat - 0.5;
				double yoffset = (double) yh/(double) repeat - 0.5;
				
				xoffset *= dispersion;
				yoffset *= dispersion;
				
				//Convert the point into global space
				double dxPointAndCoordinate = (double)ix + (0.5 + xoffset) - xf;
				double dyPointAndCoordinate = (double)iy + (0.5 + yoffset) - yf;
				
				double cpx = globalx + dxPointAndCoordinate * scale;
				double cpy = globaly + dyPointAndCoordinate * scale;
				
				px[(ix+2)+5*(iy+2)] = cpx;
				py[(ix+2)+5*(iy+2)] = cpy;
				
				//Get the height of the point
				ph[(ix+2)+5*(iy+2)] = baseNoise.cachedGetValueAt((float) cpx, (float) cpy);
			}
		}
		
		//Connect points to lowest neighbour
		int[] pc = new int[25];
		for(int i = 0; i < 25; i++) {
			pc[i] = i;
		}
		for(int ix = 0; ix < 5; ix++) {
			for(int iy = 0; iy < 5; iy++) {
				
				double lowestHeight = 10000;
				
				for(int sx = -1; sx < 2; sx++) {
					for(int sy = -1; sy < 2; sy++) {
						
						int cx = Math.max(0, Math.min(4, (ix+sx)));
						int cy = Math.max(0, Math.min(4, (iy+sy)));
						
						double h = ph[cx + 5*cy];
						
						if(h < lowestHeight) {
							lowestHeight = h;
							pc[ix + 5*iy] = cx+5*cy;
						}
						
					}
				}
			}
		}
		
		//Iterate over the closest points
		double height = 10000.0;
		for(int ix = 1; ix < 4; ix++) {
			for(int iy = 1; iy < 4; iy++) {
				double x1 = px[ix+5*iy];
				double y1 = py[ix+5*iy];
				
				double x2 = px[pc[ix+5*iy]];
				double y2 = py[pc[ix+5*iy]];
				
				double f1 = ((y1-y2)*(globaly-y1)+(x1-x2)*(globalx-x1))/(sqr(y1-y2)+sqr(x1-x2));
				double f2 = Math.abs(((y1-y2)*(globalx-x1)-(x1-x2)*(globaly-y1))/sqrt(sqr(x1-x2)+sqr(y1-y2)));
				
				double eh = 0;
				if(f1 > 0.0) {
					eh = sqrt(sqr(globalx-x1)+sqr(globaly-y1));
				}
				else if(f1 < -1.0) {
					eh = sqrt(sqr(globalx-x2)+sqr(globaly-y2));
				}
				else {
					eh = f2;
				}
				
				if(eh < height) {
					height = eh;
				}
			}
		}
		
		height /= scale * 0.5;
		
		return height;
	}
	
	private double sqrt(double a) {
		return Math.sqrt(a);
	}
	
	private double sqr(double a) {
		return a * a;
	}
	
	private int hashX(int x, int y) {
		int h = dp[dp[x]+y];
		return h;
	}
	private int hashY(int x, int y) {
		int h = dp[dp[y]+x];
		return h;
	}
	
	private int inc(int num, int n) {
		num += n;
		int ret;
		if(num >= 0) ret = num % repeat;
		else ret = (repeat-1)+((num+1)%repeat);
		return ret;
	}
}
