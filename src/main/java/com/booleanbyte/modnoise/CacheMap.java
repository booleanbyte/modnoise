package com.booleanbyte.modnoise;

import java.util.LinkedHashMap;
import java.util.Map;

public class CacheMap {
	private final LinkedHashMap<String, Float> cachemap;
	
	public CacheMap(int size) {
		cachemap = new LinkedHashMap<String, Float>() {
			private static final long serialVersionUID = -4627360031439501390L;

			@Override
			protected boolean removeEldestEntry(final Map.Entry<String, Float> eldest) {
				return size() > size;
			}
		};
	}
	
	public void put(float value, float x, float y) {
		String key = "(" + x + "," + y + ")";
		cachemap.put(key, value);
	}
	
	public Float get(float x, float y) {
		return cachemap.get("(" + x + "," + y + ")");
	}
}
