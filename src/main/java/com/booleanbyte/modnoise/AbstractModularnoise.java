package com.booleanbyte.modnoise;

public abstract class AbstractModularnoise {
	
	CacheMap cachemap = null;
	
	public float cachedGetValueAt(float x, float y) {
		if(cachemap == null) cachemap = new CacheMap(1024);
		
		Float cachedValue = cachemap.get(x, y);
		if(cachedValue != null) return cachedValue;
		
		float value = getValueAt(x, y);
		cachemap.put(value, x, y);
		return value;
	}
	
	public abstract float getValueAt(float x, float y);
}
